# README #

A simple tool for classifying collections of text in JSON format. At the top level of file should be an array. Each object in that array should have the text element.

Basic usage is: 
java -jar classifier.jar filename

The jar can be built by running ant, or you can just download the prebuilt binary.

### Usage Options ###

To see the usage options do:
java -jar classifier.jar -help

Options included allow specifying a certain number of articles to classify and what article number to start at. 
For example:
java -jar classifier.jar test2.txt -startat 3 -numarticles 2


