package classifier;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;

public class CLI {

	@Parameter(names = "-continuous", description = "Sets the classification to be continuous (ie 0.3, -0.67 etc) else it is discrete")
	boolean floatMode = false;

	@Parameter(names = "-jsontextarrayname", description = "Name of the JSON array containing the texts")
	String textArrayName = Main.DEFAULT_TEXT_ARRAY_NAME;

	@Parameter(names = "-jsontextname", description = "Name for the texts in the JSON file")
	String textName = Main.DEFAULT_TEXT_NAME;

	@Parameter(names = "-jsontitlename", description = "Name for the article titles in the JSON file")
	String titleName = Main.DEFAULT_TITLE_NAME;

	@Parameter(names = "-startat", description = "Article number to start at")
	Integer startAt = 1;

	@Parameter(names = "-numarticles", description = "Number of articles to classify")
	Integer numArticles = Integer.MAX_VALUE;

	@Parameter(names = "-noskip", description = "Don't skip already classified articles")
	boolean dontSkipClassified = false;

	@Parameter(names = "-overwrite", description = "Update the existing file. If not set will create a new file")
	boolean overWrite = false;

	@Parameter(names = "-help", help = true, description = "Displays the usage options")
	boolean help = false;

	@Parameter(description = "Filename")
	List<String> fileName = new ArrayList<String>();

	@Parameter(names = "-clean", description = "Remove any existing classifcations and write to new file. May combine with -overwrite")
	boolean clean = false;

}
