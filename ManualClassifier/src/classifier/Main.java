package classifier;

import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

import com.beust.jcommander.JCommander;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

public class Main {

	static final String DEFAULT_TEXT_ARRAY_NAME = "articles";
	static final String DEFAULT_TEXT_NAME = "articleBody";
	static final String CLASSIFIER_TAG = "sentiment";
	static final String DEFAULT_TITLE_NAME = "title";

	private static CLI cli;

	public static void main(String[] args) {

		cli = new CLI();
		// String[] testArgs = { "test2.txt", "-overwrite", "-startat", "3",
		// "-noskip","-clean" };

		JCommander j = new JCommander(cli, args);

		// Display help if set
		if (cli.help) {
			j.setProgramName("Manual Classifier");
			j.usage();
			System.exit(0);
		}

		if (cli.fileName.isEmpty()) {
			System.out.println("A filename is required. Use -help to display the options");
			System.exit(0);
		}

		if (cli.clean) {
			cleanFile(cli.fileName.get(0));
			System.exit(0);
		}

		doStuff(cli.fileName.get(0));

	}

	private static void doStuff(String fileName) {

		JsonElement jelement = new JsonParser().parse(getFileReader(fileName));
		JsonObject jobject = jelement.getAsJsonObject();

		if (cli.floatMode) {
			System.out.println("Continuous mode is set");
		} else {
			System.out.println("Discrete mode is set");
			System.out.println("Use p for positive, n for negative and z for neutral classification");
		}

		System.out.println("Use q to quit and save the results");
		System.out.println("Press enter to start");

		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();

		JsonArray textArray = jobject.getAsJsonArray(cli.textArrayName);

		int articlesRemaining = cli.numArticles;
		int curArticle = 0;

		for (JsonElement e : textArray) {

			curArticle++;

			// Skip out articles if necessary
			if (curArticle < cli.startAt || articlesRemaining <= 0) {
				continue;
			}

			JsonObject o = e.getAsJsonObject();

			// Skip out already classified articles
			if (!cli.dontSkipClassified && o.get(CLASSIFIER_TAG) != null) {
				continue;
			}

			articlesRemaining--;

			// Print out the title
			if (o.get(cli.titleName) != null) {
				System.out.println(o.get(cli.titleName).getAsString());
			}

			// Print out the text
			System.out.println(o.get(cli.textName).getAsString());

			System.out.println();
			System.out.println("Enter classification for article " + curArticle);

			float v = getClassification(scanner);

			// Quit
			if (v == -2) {
				break;
			}

			// Add the new sentiment value
			o.add(CLASSIFIER_TAG, new JsonPrimitive(v));

		}

		scanner.close();

		System.out.println("Classification finished");

		// Convert back to a string and write to file
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(jelement);

		if (!cli.overWrite) {
			fileName += "_sentiment";
		}

		writeToJsonFile(fileName, json, cli.overWrite);

	}

	/**
	 * Get the next classification. Returns -2 if quit was called
	 */
	private static float getClassification(Scanner scanner) {

		float v = -2;

		while (v == -2) {

			String input = scanner.nextLine();
			if (input.equals("q")) {
				System.out.println("Quitting...");
				break;
			}

			if (cli.floatMode) {
				try {
					v = Float.valueOf(input);
				} catch (NumberFormatException e) {
					System.out.println("Please enter a number. Use q to quit");
				}
			} else {
				if (input.equals("p")) {
					v = 1f;
				} else if (input.equals("n")) {
					v = -1f;
				} else if (input.equals("z")) {
					v = 0f;
				} else {
					System.out.println("Please enter either p,n or z. Use q to quit");
				}
			}

		}
		return v;

	}

	private static void cleanFile(String fileName) {

		System.out.println("Cleaning file: " + fileName);

		JsonElement jelement = new JsonParser().parse(getFileReader(fileName));
		JsonObject jobject = jelement.getAsJsonObject();

		JsonArray textArray = jobject.getAsJsonArray(cli.textArrayName);

		for (JsonElement e : textArray) {
			JsonObject o = e.getAsJsonObject();
			o.remove(CLASSIFIER_TAG);
		}

		// Convert back to a string and write to file
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(jelement);

		if (!cli.overWrite) {
			fileName += "_cleaned";
		}

		writeToJsonFile(fileName, json, cli.overWrite);

	}

	private static void writeToJsonFile(String fileName, String json, boolean overWrite) {

		// If we aren't overwriting current file, find an untaken filename
		if (!overWrite) {
			String extra = "";
			int e = 1;
			while (Files.exists(Paths.get(fileName + extra))) {
				extra = String.valueOf(e);
				e++;
			}

			fileName += extra;
		}

		try {
			if (overWrite) {
				Files.write(Paths.get(fileName), json.getBytes(StandardCharsets.UTF_8));
			} else {
				Files.write(Paths.get(fileName), json.getBytes(StandardCharsets.UTF_8),
						StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
			}
			System.out.println("Wrote file: " + fileName);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error writing file: " + fileName);
		}

	}

	private static Reader getFileReader(String fileName) {
		Reader reader = null;
		try {
			reader = Files.newBufferedReader(Paths.get(fileName), StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Error reading file: " + fileName);
			System.exit(1);
		}

		return reader;
	}
}
